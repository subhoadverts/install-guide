:experimental:

[[sect-installation-gui-root-password]]
=== Root Password

The `Root Password` screen is used to configure the `root` password for your system. This password will be used to log into the administrator (also known as superuser) account, which is used for system administration tasks such as installing and updating software packages and changing system-wide configuration such as network and firewall settings, storage options and adding or modifying users, groups and file permissions.

The `root` account will always be created during the installation. However, you should always also create a normal user account in xref:install/Installing_Using_Anaconda.adoc#sect-installation-gui-create-user[Create User] and use that account to log in to the system. Only switch to the administrator account only when you need to perform a task which requires administrator access.

[WARNING]
====

The `root` account has complete control over the system. If an unauthorized person gains access to this account, they can access or delete all users’ personal files or otherwise exploit the machine for their own nefarious purposes. See the [citetitle]_{PRODUCT} Security Guide_, available at link:++https://docs.fedoraproject.org/++[], for detailed documentation about account security and guidelines for choosing a strong password.

====

.Root Password

image::anaconda/PasswordSpoke.png[The Root Password screen. Use the text input fields to provide your root password.]

Once you choose a strong password, enter it in the `Root Password` field. The characters you write will be displayed as dots for security. Then, type the same password into the `Confirm` field to ensure you entered it properly. Both entered passwords must be the same.

As you enter the password, it will be evaluated and the installer will determine the password's strength. If the installer considers your password weak, a message will appear at the bottom of the screen, explaining which aspect of your chosen password is considered insuficient. For example:

[subs="quotes, macros"]
----
`The password you have provided is weak:` `The password is shorter than 5 characters.`
----

If a message similar to the above appears, it is highly recommended to choose a different, stronger password.

Once you configure the superuser password, click `Done` in the top left corner to return to xref:install/Installing_Using_Anaconda.adoc#sect-installation-gui-installation-progress[Configuration and Installation Progress]. If you selected a weak password, you must press the button twice.
